This is a read me for the LISP Tic Tac Toe Project game.

The way this works will be that once you run the file, the play function will start
a game where you can play as an user versus a computer. The computer will make at
first random moves to start until it finds a pattern where you can potentially win.

First, as soon as you start playing, the board will be empty. The user and the computer
will play until the total amount of moves played is 5, then the check if you won function will
take place after each turn to make sure someone won.  This function will run until the board is 
full or someone has won.  If both the board is full and no one won, the check if you won function
will decide its a draw and will end the game saying its a draw. Also, everytime a move is played,
a check if the move is legal function will also take place, that way no one can doule down on
their moves or make an illegal move. 

Making the player move function will have other functions that will help the operation run.  Making 
sure the move is legal, check if you won and print the board so you can see it updated everytime.
The computer part is more complicated because there are many instances that the computer has
to know for it to run logically.  Since the users brains already does that, there is no need
to apply those functions here.  The first one, is to just make a random move, then another 
function that tries to win the game by making 3 in a row, another one is to block moves that
the user could do to win and choosing if you want to win or block.

Once the computer and user are able to play, the function to check for winning has to 
see if one of the eigth scenarios that are possible in tic tac toe has happened and 
if not, then its a tie. Therefore, we need to create each scenario in the function to 
check if that board matches any of them.
