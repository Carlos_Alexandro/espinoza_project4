;;
;; Functions for tic tac toe

;; A sample board to play with and test our functions
(defparameter *sampleboard* (list 'x 'x 'x '- 'o '- '- 'o '-))

;;
;; Print a board
;;
(defun print-board (board)
  (format t "=============")
  (do ( (i 0 (+ i 1)))
      ( (= i 9) 'done)
      ;; each of the following expressions is evaled every iteration
      (if ( = (mod i 3) 0) (format t "~%|") nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)


;; Determines if all three symbols are the same
(defun threequal (a b c)
  (and (equal a b) (equal b c)))

(defun threequalist (listofthree)
  (and (equal (first listofthree) (second listofthree))
       (equal (second listofthree) (third listofthree))))

(defun old-grabrow (board row)
  (list (nth (+ 0 (* row 3)) board)
        (nth (+ 1 (* row 3)) board)
        (nth (+ 2 (* row 3)) board))
)


(defun echoplustwo ()
  (princ "type a number: ")
  (let ( (x (+ (read) 2)) )
    (format t "You typed ~A~%" x)
    ;;(write x)
    (print-board *sampleboard*)
  )
)

(defun grabrow (board row)
  (let ( (x (* row 3)) )
    (list (nth (+ 0 x) board)
          (nth (+ 1 x) board)
          (nth (+ 2 x) board))))

(defun grabcol (board col)
  (list (nth (+ 0 col) board)
        (nth (+ 3 col) board)
        (nth (+ 6 col) board)))
        
(defun grabdiag (board whichone)
  (if (equal whichone 'upperleft)
      (list (nth 0 board) (nth 4 board) (nth 8 board))
      (list (nth 2 board) (nth 4 board) (nth 6 board))))

;;///////////////////////////////////////////////////////////


;; Function that will call the playerTurn function to start the game
;; which grabs a board made from the makeBoard function to start
;; at an empty board.  
(defun startGame()
  (print "Game will start now! You are the X")
  (playerTurn(makeBoard))
)


;; Making an empty board
(defun makeBoard()
  (list 'board 0 0 0 0 0 0 0 0 0)
)

;; Making a function that takes in the current board, 
;; the player (CP or User) and the position they want.
;; This changes the board according to the input
;; But this only happens once the move has been approved.
(defun makeMove(board player posi)
  (setf (nth pos board) player) 
    board
)
;; I know this is not the way you wanted us to do it, but ill fix it by 
;; the next iteration.



;; This function takes the current board to see if its a winning board
;; with one of the 8 scenarios that you can win with.
(defun checkForWin(board)

  (let (var *threes* 
    '(
        (1 2 3) (4 5 6) (7 8 9)   ;; This is the horizontal wins
        (1 4 7) (2 5 8) (3 6 9)   ;; This is the veritcal wins
        (1 5 9) (3 5 7)           ;; This is the cross wins
    )
  ))
  
  (or (loop for threes in threes
          when (winCondition threes board) return (nth threes) board
  (if (checkBoardFull) 0)
  ))

)

;; This function checks if the board has a winning combination
(defun winCondition(threes board)
  (let
    (+
      (nth (first threes) board)
      (nth (second threes) board)
      (nth (third threes) board)
    )
  )
  ;; Checks if each First, Second and Third object of the given board
  ;; Match to the winnning conditions of the checkForWin function.
  ;; Therefore, adding the board values to another list and then
  ;; Comparing.
  (and first second third (= first second third)) 
)


;; This function checks if move is illegal in the current board
;; Checks if there is already another value of O or X in there.
(defun illegalMove (board posi)

)

;; This function only checks if the board is full by seeing if there are
;; any 0 left in there which means a space still is left.
(defun checkBoardFull(board)
  (not (member 0 board))
)
        
;; Makes the computer do a random move from 1 to 9
;; If that place is taken, then rerun the function until a spot is found
;; This should only be called upon around 2, max 3 times during the game.
(defun cpRandomMove (board)
    (let ((posi (+ 1 (random 9))))      ;; Makes sure the number spot is not 0
        (if (zerop (nth pos board))
            (cpRandomMove board)
        )
    )
)
;; This function grabs the board as its parameter and allows the 
;; player to give a position on the board and goes through all the 
;; hoops to check if its legal, if you won, if you tied or need to 
;; keep going and switches turns to the Computer
(defun playerTurn(board)
  (setq counter 0)
  (setq counter (+ counter 1))
  (print "Make your move!")
    (
      (checkBoardFull(board))  ;; Check if board is full
      (let* (posi (illegalMove board)) ;; Tests if the players move is legal
        (new_board (makeMove *player* posi board)) ;; If not, make another move
      )
      (print new_board) ;; Prints the new board
      (cond ((checkForWin new_board)  (Print "You won!")) ;; Calls the function to check if you won
            ((checkBoardFull new_board)  (Print "Board is full, its a tie!")) ;; If board is full, call it a tie
            (t (cpTurn new_board))   ;; Make sure the last thing we do is
                                     ;; change turns to the cp.
      )
    )
)


(cpTurn (board)
  (print "Computers turn!")
    (
      (checkBoardFull(board))
      (if (< counter 5) 
        then cpRandomMove(board) )

      (let* (posi (illegalMove board)) ;; Tests if the players move is legal
        (new_board (makeMove *player* posi board)) ;; If not, make another move
      )



    )



)



;; Calling on the start game function to get the game going.
(startGame)
